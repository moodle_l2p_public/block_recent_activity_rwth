<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * class block_recent_activity_rwth
 *
 * @package    block_recent_activity_rwth
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/course/lib.php');

/**
 * class block_recent_activity_rwth
 *
 * @package    block_recent_activity_rwth
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_recent_activity_rwth extends block_base {

    const RECENT_PERIOD_SINCE_LAST_LOGIN = "0";
    const DEFAULT_RECENT_PERIOD = self::RECENT_PERIOD_SINCE_LAST_LOGIN;

    /**
     * Use {@link block_recent_activity_rwth::get_timestart()} to access
     *
     * @var int stores the time since when we want to show recent activity
     */
    protected $timestart = null;

    /**
     * Initialises the block
     */
    function init() {
        $this->title = get_string('pluginname', 'block_recent_activity_rwth');
    }

    /**
     * Returns the content object
     *
     * @return stdObject
     */
    function get_content() {
        if ($this->content !== NULL) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if ($this->is_single_course()) {
            $course = $this->page->course;
            $courses = [$course->id => $course];
            $structuralchanges = [$course->id => $this->get_structural_changes_for_course($course)];
            $modulesrecentactivity = [$course->id => $this->get_modules_recent_activity_rwth_for_course($course)];
        } else {
            $courses = enrol_get_my_courses();
            $structuralchanges = $this->get_structural_changes_for_courses($courses);
            $modulesrecentactivity = $this->get_modules_recent_activity_rwth_for_courses($courses);
        }

        $renderer = $this->page->get_renderer('block_recent_activity_rwth');
        $this->content->text = $renderer->recent_activity_rwth(
                $structuralchanges,
                $modulesrecentactivity,
                $this->get_recent_periods(),
                $this->get_recent_period(),
                $courses);

        return $this->content;
    }

    /**
     * @return int recent_period (in DateInterval syntax)
     */
    public function get_recent_period() {
        global $SESSION;
        $recentperiod = self::DEFAULT_RECENT_PERIOD;
        if (isset($SESSION->block_recent_activity_rwth_recentperiod)) {
            $recentperiod = $SESSION->block_recent_activity_rwth_recentperiod;
        }
        $recentperiod = optional_param('recentperiod', $recentperiod, PARAM_ALPHANUM);
        $SESSION->block_recent_activity_rwth_recentperiod = $recentperiod;
        return $recentperiod;
    }

    /**
     * @return array selectable recent periods (seconds => description text)
     */
    public function get_recent_periods() {
        global $USER;
        $course = $this->page->course;
        $ret = [];
        if (!isguestuser()) {
            $timestart = time() - self::get_max_recent_period();
            $lastaccess = null;
            if ($course->id == SITEID) {
                if (isset($USER->lastlogin)) {
                    $lastaccess = $USER->lastlogin;
                } else {
                    // User's first login.
                    $lastaccess = time();
                }
            } else if (array_key_exists($course->id, $USER->lastcourseaccess)) {
                $lastaccess = $USER->lastcourseaccess[$course->id];
            } else if (array_key_exists($course->id, $USER->currentcourseaccess)) {
                $lastaccess = $USER->currentcourseaccess[$course->id];
            }
            if (!empty($lastaccess)) {
                if ($lastaccess > $timestart) {
                    $timestart = $lastaccess;
                }
            }

            $timespan = time() - $timestart;
            $today = (date('d.m.Y', time()) === date('d.m.Y', $timestart));
            $yesterday = (date('d.m.Y', time() - DAYSECS) === date('d.m.Y', $timestart));
            $thisyear = (date('Y', time()) === date('Y', $timestart));

            $timeformat = '%d. %B %Y';
            // Hide year if timestart is in current year.
            if ($thisyear) {
                $timeformat = '%d. %B';
            }
            // Show day if timestart is less then 4 weeks ago.
            if ($timespan < 4 * WEEKSECS) {
                $timeformat = '%A, %d. %B';
                // Hide day-of-week if timestart is today or yesteray.
                if ($today || $yesterday) {
                    $timeformat = '%H:%M';
                }
            }            

            if ($course->id == SITEID) {
                $stringid = 'recent_period_since_last_login';
            } else {
                $stringid = 'recent_period_since_last_course_access';
            }
            // Add 'today' or 'yesterday' to date if necessary.
            $date = userdate($timestart, $timeformat);
            if ($today) {
                $date = get_string('today', 'block_recent_activity_rwth') . ', ' . $date;
            } else if ($yesterday) {
                $date = get_string('yesterday', 'block_recent_activity_rwth') . ', ' . $date;
            }
            $ret[self::RECENT_PERIOD_SINCE_LAST_LOGIN] = get_string($stringid, 'block_recent_activity_rwth', $date);
        }

        $periods = [];
        $periodscfg = get_config('block_recent_activity_rwth', 'recent_periods');
        if ($periodscfg) {
            $periods = explode(',', $periodscfg);
        }

        $recentperiod = $this->get_recent_period();
        if ($recentperiod != self::RECENT_PERIOD_SINCE_LAST_LOGIN
                && !in_array($recentperiod, $periods)) {
            $periods[] = $recentperiod;
        }

        $now = time();
        foreach ($periods as $period) {
            try {
                $diff = new DateInterval($period);
            } catch (Exception $e) {
                debugging('<h3>'.$e->getMessage().'</h3><pre>'.$e->getTraceAsString().'</pre>');
                continue;
            }
            $periodparts = [];

            $years = $diff->format('%y');
            if ($years > 1) {
                $periodparts[] = $years.' '.get_string('years', 'block_recent_activity_rwth');
            } else if ($years == 1) {
                $periodparts[] = '1 '.get_string('year', 'block_recent_activity_rwth');
            }

            $months = $diff->format('%m');
            if ($months > 1) {
                $periodparts[] = $months.' '.get_string('months', 'block_recent_activity_rwth');
            } else if ($months == 1) {
                $periodparts[] = '1 '.get_string('month', 'block_recent_activity_rwth');
            }

            $days = $diff->format('%d');
            $weeks = floor($days / 7);
            $days -= $weeks * 7;
            if ($weeks > 1) {
                $periodparts[] = $weeks.' '.get_string('weeks', 'block_recent_activity_rwth');
            } else if ($weeks == 1) {
                $periodparts[] = '1 '.get_string('week', 'block_recent_activity_rwth');
            }
            if ($days > 1) {
                $periodparts[] = $days.' '.get_string('days', 'block_recent_activity_rwth');
            } else if ($days == 1) {
                $periodparts[] = '1 '.get_string('day', 'block_recent_activity_rwth');
            }

            $hours = $diff->format('%h');
            if ($hours > 1) {
                $periodparts[] = $hours.' '.get_string('hours', 'block_recent_activity_rwth');
            } else if ($hours == 1) {
                $periodparts[] = '1 '.get_string('hour', 'block_recent_activity_rwth');
            }

            $minutes = $diff->format('%i');
            if ($minutes > 1) {
                $periodparts[] = $minutes.' '.get_string('minutes', 'block_recent_activity_rwth');
            } else if ($minutes == 1) {
                $periodparts[] = '1 '.get_string('minute', 'block_recent_activity_rwth');
            }

            $seconds = $diff->format('%s');
            if ($seconds > 1) {
                $periodparts[] = $seconds.' '.get_string('seconds', 'block_recent_activity_rwth');
            } else if ($seconds == 1) {
                $periodparts[] = '1 '.get_string('second', 'block_recent_activity_rwth');
            }
            $periodstr = get_string('since', 'block_recent_activity_rwth').' '.implode(' ', $periodparts);
            $ret[$period] = $periodstr;
        }
        return $ret;
    }

    /**
     * @return int max_recent_period (provided in seconds)
     */
    public static function get_max_recent_period() {
        $now = time();
        $d = new DateTime();
        $d->setTimestamp($now);
        $maxperiodcfg = get_config('block_recent_activity_rwth', 'max_recent_period');
        try {
            $d->sub(new DateInterval($maxperiodcfg));
        } catch (Exception $e) {
            debugging('<h3>'.$e->getMessage().'</h3><pre>'.$e->getTraceAsString().'</pre>');
            // Use 6 months as fallback.
            $d->sub(new DateInterval('P6M'));
        }
        return $now - $d->getTimestamp();
    }

    /**
     * Returns the time since when we want to show recent activity
     *
     * For guest users it is 2 days, for registered users it is the time of last access to the course
     *
     * @return int
     */
    protected function get_timestart() {
        global $USER, $PAGE;
        $course = $this->page->course;
        if ($this->timestart === null) {
            $this->timestart = round(time() - self::get_max_recent_period(), -2); // better db caching for guests - 100 seconds

            if (!isguestuser()) {
                $recentperiod = $this->get_recent_period();
                if ($recentperiod === self::RECENT_PERIOD_SINCE_LAST_LOGIN) {
                    $lastaccess = null;
                    if ($course->id == SITEID) {
                        $lastaccess = $USER->lastlogin;
                    } else if (array_key_exists($course->id, $USER->lastcourseaccess)) {
                        $lastaccess = $USER->lastcourseaccess[$course->id];
                    } else if (array_key_exists($course->id, $USER->currentcourseaccess)) {
                        $lastaccess = $USER->currentcourseaccess[$course->id];
                    }
                    if (!empty($lastaccess)) {
                        if ($lastaccess > $this->timestart) {
                            $this->timestart = $lastaccess;
                        }
                    }
                } else {
                    try {
                        $time = new DateTime();
                        $time->sub(new DateInterval($recentperiod));
                        $this->timestart = $time->getTimestamp();
                    } catch (Exception $e) {
                        debugging('<h3>'.$e->getMessage().'</h3><pre>'.$e->getTraceAsString().'</pre>');
                        $this->timestart = time() - self::get_max_recent_period();
                    }
                }
            }
        }
        return $this->timestart;
    }

    protected function get_structural_changes_for_courses($courses) {
        $courses = enrol_get_my_courses();
        $changelist = [];
        foreach ($courses as $course) {
            $tmp = $this->get_structural_changes_for_course($course);
            if (!is_array($tmp) || empty($tmp)) {
                continue;
            }
            $changelist[$course->id] = $tmp;
        }
        return $changelist;
    }

    /**
     * Returns list of recent changes in course structure
     *
     * It includes adding, editing or deleting of the resources or activities
     * Excludes changes on modules without a view link (i.e. labels), and also
     * if activity was both added and deleted
     *
     * @return array array of changes. Each element is an array containing attributes:
     *    'action' - one of: 'add mod', 'update mod', 'delete mod'
     *    'module' - instance of cm_info (for 'delete mod' it is an object with attributes modname and modfullname)
     */
    protected function get_structural_changes_for_course($course) {
        global $DB;
        $context = context_course::instance($course->id);
        $canviewdeleted = has_capability('block/recent_activity_rwth:viewdeletemodule', $context);
        $canviewupdated = has_capability('block/recent_activity_rwth:viewaddupdatemodule', $context);
        if (!$canviewdeleted && !$canviewupdated) {
            return;
        }

        $timestart = $this->get_timestart();
        $changelist = array();
        // The following query will retrieve the latest action for each course module in the specified course.
        // Also the query filters out the modules that were created and then deleted during the given interval.
        $sql = "SELECT
                    cmid, MIN(action) AS minaction, MAX(action) AS maxaction, MAX(modname) AS modname, timecreated AS showtiming
                FROM {block_recent_activity_rwth}
                WHERE timecreated > ? AND courseid = ?
                GROUP BY cmid
                ORDER BY MAX(timecreated) ASC";
        $params = array($timestart, $course->id);
        $logs = $DB->get_records_sql($sql, $params);
        if (isset($logs[0])) {
            // If special record for this course and cmid=0 is present, migrate logs.
            self::migrate_logs($course);
            $logs = $DB->get_records_sql($sql, $params);
        }
        if ($logs) {
            $modinfo = get_fast_modinfo($course);
            foreach ($logs as $log) {
                // We used aggregate functions since constants CM_CREATED, CM_UPDATED and CM_DELETED have ascending order (0,1,2).
                $wasdeleted = ($log->maxaction == block_recent_activity_rwth_observer::CM_DELETED);
                $wascreated = ($log->minaction == block_recent_activity_rwth_observer::CM_CREATED);

                if ($wasdeleted && $wascreated) {
                    // Activity was created and deleted within this interval. Do not show it.
                    continue;
                } else if ($wasdeleted && $canviewdeleted) {
                    if (plugin_supports('mod', $log->modname, FEATURE_NO_VIEW_LINK, false)) {
                        // Better to call cm_info::has_view() because it can be dynamic.
                        // But there is no instance of cm_info now.
                        continue;
                    }
                    // Unfortunately we do not know if the mod was visible.
                    $modnames = get_module_types_names();
                    $changelist[$log->cmid] = array('action' => 'delete mod',
                        'module' => (object)array(
                            'modname' => $log->modname,
                            'modfullname' => isset($modnames[$log->modname]) ? $modnames[$log->modname] : $log->modname
                        ),
                         'time' => $log->showtiming
                        );

                } else if (!$wasdeleted && isset($modinfo->cms[$log->cmid]) && $canviewupdated) {
                    // Module was either added or updated during this interval and it currently exists.
                    // If module was both added and updated show only "add" action.
                    $cm = $modinfo->cms[$log->cmid];
                    $section = $modinfo->get_section_info($cm->sectionnum);
                    if ($cm->has_view() && $cm->uservisible && $section->visible) {
                        $changelist[$log->cmid] = array(
                            'action' => $wascreated ? 'add mod' : 'update mod',
                            'module' => $cm,
                            'time' => $log->showtiming
                        );
                    }
                }
            }
        }
        return $changelist;
    }

    protected function get_modules_recent_activity_rwth_for_courses($courses) {
        $recentactivity = [];
        foreach ($courses as $course) {
            $tmp = $this->get_modules_recent_activity_rwth_for_course($course);
            foreach ($tmp as $key => $value) {
                if (!array_key_exists($course->id, $recentactivity)) {
                    $recentactivity[$course->id] = [];
                }
                if (array_key_exists($key, $recentactivity[$course->id])) {
                    $recentactivity[$course->id][$key] .= $value;
                } else {
                    $recentactivity[$course->id][$key] = $value;
                }
            }
        }
        return $recentactivity;
    }

    /**
     * Returns list of recent activity within modules
     *
     * For each used module type executes callback MODULE_print_recent_activity_rwth()
     *
     * @return array array of pairs moduletype => content
     */
    protected function get_modules_recent_activity_rwth_for_course($course) {
        $context = context_course::instance($course->id);
        $viewfullnames = has_capability('moodle/site:viewfullnames', $context);
        $hascontent = false;

        $modinfo = get_fast_modinfo($course);
        $usedmodules = $modinfo->get_used_module_names();
        $recentactivity = array();
        foreach ($usedmodules as $modname => $modfullname) {
            // Each module gets it's own logs and prints them
            ob_start();
            $hascontent = component_callback('mod_'. $modname, 'print_recent_activity',
                    array($course, $viewfullnames, $this->get_timestart()), false);
            if ($hascontent) {
                $contents = ob_get_contents();

                if (empty($contents)) {
                    break;
                }
                $contents = preg_replace_callback(
                    '#<div class="info.*?</div>#',
                    function($matches) {
                        $content = $matches[0];
                        // Remove boldness.
                        $content = str_replace('class="info bold"', 'class="info"', $content);

                        // Add line clamping (max two lines).
                        $content = str_replace('class="info"', 'class="info line-clamp-2"', $content);

                        // Add title.
                        $content = preg_replace_callback(
                            '#<a.*?>(.*?)</a>#',
                            function($matches2) {
                                $title = $matches2[1];
                                return str_replace('<a', '<a title="'.htmlentities($title).'"', $matches2[0]);
                            },
                            $content);
                        return $content;
                    },
                    $contents);
                // Remove margin below last tag with class "info".
                $search = 'class="info';
                $replace = 'class="info last';
                $pos = strrpos($contents, $search);
                if ($pos !== false) {
                    $contents = substr_replace($contents, $replace, $pos, strlen($search));
                }

                $recentactivity[$modname] = $contents;
            }
            ob_end_clean();
        }
        
        return $recentactivity;
    }

    /**
     * Which page types this block may appear on.
     *
     * @return array page-type prefix => true/false.
     */
    function applicable_formats() {
        return array('all' => true, 'my' => true, 'tag' => false);
    }

    /**
     * Remove old entries from table block_recent_activity_rwth
     */
    public function cron() {
        global $DB;
        // Those entries will never be displayed as RECENT anyway.
        $DB->delete_records_select('block_recent_activity_rwth', 'timecreated < ?',
                array(time() - self::get_max_recent_period()));
    }

    /**
     * Migrates entries from table {log} into {block_recent_activity_rwth}
     *
     * We only migrate logs for the courses that actually have recent activity
     * block and that are being viewed within self::get_max_recent_period() time
     * after the upgrade.
     *
     * The presence of entry in {block_recent_activity_rwth} with the cmid=0 indicates
     * that the course needs log migration. Those entries were installed in
     * db/upgrade.php when the table block_recent_activity_rwth was created.
     *
     * @param stdClass $course
     */
    protected static function migrate_logs($course) {
        global $DB;
        if (!$logstarted = $DB->get_record('block_recent_activity_rwth',
                array('courseid' => $course->id, 'cmid' => 0),
                'id, timecreated')) {
            return;
        }
        $DB->delete_records('block_recent_activity_rwth', array('id' => $logstarted->id));
        try {
            $logs = $DB->get_records_select('log',
                    "time > ? AND time < ? AND course = ? AND
                        module = 'course' AND
                        (action = 'add mod' OR action = 'update mod' OR action = 'delete mod')",
                    array(time() - self::get_max_recent_period(), $logstarted->timecreated, $course->id),
                    'id ASC', 'id, time, userid, cmid, action, info');
        } catch (Exception $e) {
            // Probably table {log} was already removed.
            return;
        }
        if (!$logs) {
            return;
        }
        $modinfo = get_fast_modinfo($course);
        $entries = array();
        foreach ($logs as $log) {
            $info = explode(' ', $log->info);
            if (count($info) != 2) {
                continue;
            }
            $modname = $info[0];
            $instanceid = $info[1];
            $entry = array('courseid' => $course->id, 'userid' => $log->userid,
                'timecreated' => $log->time, 'modname' => $modname);
            if ($log->action == 'delete mod') {
                if (!$log->cmid) {
                    continue;
                }
                $entry['action'] = 2;
                $entry['cmid'] = $log->cmid;
            } else {
                if (!isset($modinfo->instances[$modname][$instanceid])) {
                    continue;
                }
                if ($log->action == 'add mod') {
                    $entry['action'] = 0;
                } else {
                    $entry['action'] = 1;
                }
                $entry['cmid'] = $modinfo->instances[$modname][$instanceid]->id;
            }
            $entries[] = $entry;
        }
        $DB->insert_records('block_recent_activity_rwth', $entries);
    }

    /**
     * This plugin has global settings.
     */
    public function has_config() {
        return true;
    }

    protected function is_single_course() {
        return $this->page->course->id != SITEID;
    }

    function html_attributes() {
        $attributes = parent::html_attributes();
        if ($this->is_single_course()) {
            $attributes['class'] .= ' single-course';
        }
        return $attributes;
    }
}


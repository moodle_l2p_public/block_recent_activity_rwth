<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for block recent_activity
 *
 * @package    block_recent_activity_rwth
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * recent_activity_rwth block rendrer
 *
 * @package    block_recent_activity_rwth
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_recent_activity_rwth_renderer extends plugin_renderer_base {

    /**
     * Renders HTML to display recent_activity_rwth block
     *
     * @param array $structuralchanges array of changes in course structure
     * @param array $modulesrecentactivity array of changes in modules (provided by modules)
     * @param array $recentperiods possible values for recent period
     * @param mixed $recentperiod current value for recent period
     * @param array $courses courses the user is enroled in
     * @return string
     */
    public function recent_activity_rwth($structuralchanges, $modulesrecentactivity, $recentperiods, $recentperiod, $courses) {
        global $DB, $OUTPUT, $PAGE;
        $this->page->requires->js_amd_inline("require(['jquery', 'theme_boost/bootstrap/tooltip'], function($){
            $('[data-toggle=\"tooltip\"]').tooltip();
        });");

        $select = new single_select($PAGE->url, 'recentperiod', $recentperiods, $recentperiod, null, 'recent_period');
        $output = html_writer::tag('div', $OUTPUT->render($select), array('class' => 'activityhead'));

        $content = false;

        // Merge both input arrays into a new one where the changes are grouped by course.
        $coursechanges = [];
        foreach ($structuralchanges as $courseid => $structuralchanges) {
            $coursechanges[$courseid] = new stdClass;
            $coursechanges[$courseid]->structural = $structuralchanges;
        }
        foreach ($modulesrecentactivity as $courseid => $modulesrecentactivity) {
            if (!array_key_exists($courseid, $coursechanges)) {
                $coursechanges[$courseid] = new stdClass;
            }
            $coursechanges[$courseid]->modules = $modulesrecentactivity;
        }

        // Get course names etc.
        foreach ($coursechanges as $courseid => $changes) {
            $changes->course = clone($courses[$courseid]);
        }

        $ondashboard = $PAGE->pagetype == 'my-index';
        $iterator = 0;
        foreach ($coursechanges as $courseid => $changes) {
            $courseoutput = '';

            // Print a heading for the course.

            $iterator += 1;
            $toggleID = 'toggle' .  $iterator;

            if ($ondashboard) {
                $output .= '<hr />';
                $content = true;
                $coursename = get_course_display_name_for_list($changes->course);
                $text = format_text($coursename, FORMAT_MOODLE, ['para' => false]);
                $count = 0;
                if (!empty($changes->structural)) {
                    $count += count($changes->structural);
                }
                if (!empty($changes->modules)) {
                    $count += count($changes->modules);
                }

                $text .= html_writer::tag('div', '('. $count .')', ['class' => 'recent_activity_course_count_changes']);
                $courseurl = new moodle_url('/course/view.php', ['id' => $courseid]);
                $text = $OUTPUT->action_link("#$toggleID", $text, null, ['data-toggle' => 'collapse', 'class' => 'collapsed']);
                $courseoutput .= $this->heading($text, 2, 'recent_activity_course_header');
            }

            // Next, have there been any modifications to the course structure?
            $coursechangesoutput = '';
            if (!empty($changes->structural)) {
                $content = true;
                $coursechangesoutput = $this->heading(get_string("courseupdates").':', 3);

                // Add a class to the last structural change.
                end($changes->structural);
                $key = key($changes->structural);
                $changes->structural[$key]['extraclasses'] = 'last';

                foreach ($changes->structural as $changeinfo => $change) {
                    $coursechangesoutput .= $this->structural_change($change);
                }
            }

            // Now display new things from each module.
            if (!empty($changes->modules)) {
                foreach ($changes->modules as $modname => $moduleactivity) {
                    $content = true;
                   $moduleactivity = $this->replace_date_in_array($moduleactivity); // new change to add date in time elapsed format
                    $coursechangesoutput .= $moduleactivity;
                }
            }

            if ($ondashboard) {
                $class = 'recent_activity_course_changes collapse';
            } else {
                $class = 'recent_activity_course_changes';
            }
            $courseoutput .= html_writer::tag('div', $coursechangesoutput, ['class' => $class, 'id' => $toggleID, 'data-toggle' => 'collapse']);
            $output .= html_writer::tag('div', $courseoutput, ['class' => 'recent_activity_course']);
        }

        if (! $content) {
            $output .= '<hr />';
            $output .= html_writer::tag('p', get_string('nothingnew'), array('class' => 'message'));
        }
        return $output;
    }
    /**
     * Renders HTML for one change in course structure
     *
     * @see block_recent_activity_rwth::get_structural_changes()
     * @param array $change array containing attributes
     *    'action' - one of: 'add mod', 'update mod', 'delete mod'
     *    'module' - instance of cm_info (for 'delete mod' it is an object with attributes modname and modfullname)
     * @return string
     */
    protected function structural_change($change) {
        $cm = $change['module'];
        $actiondate = html_writer::tag('div', $this->get_elapsed_time('@'.$change['time']),
                ['class' => 'float-right', 'title' => userdate($change['time'], get_string('strftimerecent')), 'data-toggle' => "tooltip"]);
        switch ($change['action']) {
            case 'delete mod':
                $modaction = html_writer::tag('div', get_string('deletedactivity', 'moodle', $cm->modfullname), ['class' => 'float-left']);
                $text = $modaction.$actiondate. '<br />';
                break;
            case 'add mod':
                $linktext = format_string($cm->name, true);
                $link = html_writer::link($cm->url, $linktext,
                    ['class' => 'line-clamp-2', 'title' => $linktext]);
                $modaction = html_writer::tag('div', get_string('added', 'moodle', $cm->modfullname), ['class' => 'float-left']);
                $text = $modaction.$actiondate.'<br />'.$link.'<br />';
                break;
            case 'update mod':
                $linktext = format_string($cm->name, true);
                $link = html_writer::link($cm->url, $linktext,
                    ['class' => 'line-clamp-2', 'title' => $linktext]);
                $modaction = html_writer::tag('div', get_string('updated', 'moodle', $cm->modfullname), ['class' => 'float-left']);
                $text = $modaction.$actiondate. '<br />'.$link.'<br />';
                break;
            default:
                return '';
        }
        $classes = 'activity';
        if (isset($change['extraclasses'])) {
            $classes .= ' '.$change['extraclasses'];
        }
        return html_writer::tag('div', $text, array('class' => $classes));
    }

    /**
     * Changes the date given by various components for changes within module.
     * Extracts the date portion from the string returned by the component and
     * changes in to elapsed time format. Tooltip to prompt the date on mousehover
     * @param array string returned from the component
     * @return string
     */
    public function replace_date_in_array ($content) {
        return preg_replace_callback('|<div class="date">(?:<time[^>]*>)?(.*?)(?:</time>)?</div>|', function($matches) {
            $timestamp = $matches[1];
            $timeelapsed = html_writer::tag('div', $this->get_elapsed_time($timestamp), ['title' => $timestamp, 'data-toggle' => "tooltip"]);
            return str_replace($timestamp, $timeelapsed, $matches[0]);
        }, $content);

    }
    /**
     * Changes the date given by various components for changes within module and
     * structural changes to elapsed time: 1 day ago, 1 month ago.The datestamp is
     * passed and time difference is calculated. Based on that the function returns
     * how much time has elapsed in terms of year,month,week, day,hour,minute and
     * second. When time is less than 10 secs it gives just now.
     * @param array date stamp extracted
     * @return string
     */

    public function get_elapsed_time($datetime, $full = false) {
        // Convert German dates to english ones.
        // This isn't the cleanest solution. Replace with a better one if you find one.
        $datetime = str_replace(['Mär', 'Mai', 'Okt', 'Dez'],
                                ['Mar', 'May', 'Oct', 'Dec'],
                                $datetime);

        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago); //gives difference between now and previous time
        if ($diff->format('%y') >= 1) {
            if ($diff->format('%y') == 1) {
                $ret = get_string('yrlapsesingle', 'block_recent_activity_rwth');
            } else {
                $ret = get_string('yrslapse', 'block_recent_activity_rwth', $diff->format('%y')); //$diff->format('%y').' years ago';
            }
        } else if ($diff->format('%m') >= 1) {
            if ($diff->format('%m') == 1) {
                    $ret = get_string('mnthlapsesingle', 'block_recent_activity_rwth'); //$diff->format('%m').' month ago';
            } else {
                    $ret = get_string('mnthslapse', 'block_recent_activity_rwth', $diff->format('%m'));  //$diff->format('%m').' months ago';
            }
        }  else if ($diff->format('%d') >= 1) {
            if ($diff->format('%d') == 1) {
                    $ret = get_string('daylapsesingle', 'block_recent_activity_rwth'); //$diff->format('%d').' day ago';	
            } else if(floor($diff->format('%d') / 7) >= 1){
                if (floor($diff->format('%d') / 7) == 1){
                    $ret = get_string('weeklapsesingle', 'block_recent_activity_rwth');
                }else{
                    $ret = get_string('weekslapse', 'block_recent_activity_rwth', floor($diff->format('%d') / 7)); //floor($diff->format('%d') / 7).' weeks ago';
                }
            }
            else {
                    $ret = get_string('dayslapse', 'block_recent_activity_rwth', $diff->format('%d')); //$diff->format('%d').' days ago';
            }
        } else if ($diff->format('%h') >= 1) {
            if ($diff->format('%h') == 1) {
                    $ret = get_string('hrlapsesingle', 'block_recent_activity_rwth'); //$diff->format('%h').' hour ago';	
            } else {
                    $ret = get_string('hrslapse', 'block_recent_activity_rwth', $diff->format('%h')); //$diff->format('%h').' hours ago';
            }
        } else if ($diff->format('%i') >= 1) {
            if ($diff->format('%i') == 1) {
                    $ret = get_string('minlapsesingle', 'block_recent_activity_rwth'); //$diff->format('%i').' minute ago';	
            } else {
                    $ret = get_string('minslapse', 'block_recent_activity_rwth', $diff->format('%i')); //$diff->format('%i').' minutes ago';
            }
        } else {
            if ($diff->format('%s') < 10) {
                    $ret = get_string('seclapsesingle', 'block_recent_activity_rwth', $diff->format('%s')); //$diff->format('%s').' second ago';	
            } else {
                    $ret = get_string('secslapse', 'block_recent_activity_rwth', $diff->format('%s')); //$diff->format('%s').' second ago';
            }
        }
        return $ret;
    }
}

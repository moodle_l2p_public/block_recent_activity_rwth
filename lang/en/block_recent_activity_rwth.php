<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_recent_activity_rwth', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_recent_activity_rwth
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Recent activity';

$string['privacy:metadata'] = 'The recent activity block contains a cache of data stored elsewhere in Moodle.';
$string['privacy:metadata:block_recent_activity_rwth'] = 'Temporary log of recent teacher activity. Removed after two days';
$string['privacy:metadata:block_recent_activity_rwth:action'] = 'Action: created, updated or deleted';
$string['privacy:metadata:block_recent_activity_rwth:cmid'] = 'Course activity ID';
$string['privacy:metadata:block_recent_activity_rwth:courseid'] = 'Course ID';
$string['privacy:metadata:block_recent_activity_rwth:modname'] = 'Module type name (for delete action)';
$string['privacy:metadata:block_recent_activity_rwth:timecreated'] = 'Time when action was performed';
$string['privacy:metadata:block_recent_activity_rwth:userid'] = 'User performing the action';

$string['recent_activity_rwth:addinstance'] = 'Add a new recent activity block';
$string['recent_activity_rwth:myaddinstance'] = 'Add a new recent activity block';
$string['recent_activity_rwth:viewaddupdatemodule'] = 'View added and updated modules in recent activity block';
$string['recent_activity_rwth:viewdeletemodule'] = 'View deleted modules in recent activity block';

$string['max_recent_period'] = 'Max recent period';
$string['max_recent_period_desc'] = 'The maximum time since when to show recent activity. Format: <a href="https://www.php.net/manual/en/dateinterval.construct.php">https://www.php.net/manual/en/dateinterval.construct.php</a>';
$string['recent_periods'] = 'Timespans';
$string['recent_periods_desc'] = 'Timespans (comma separated) in which the user can see recent activity. Format: <a href="https://www.php.net/manual/en/dateinterval.construct.php">https://www.php.net/manual/en/dateinterval.construct.php</a>';
$string['recent_activity_more'] = 'more...';
$string['recent_activity_less'] = 'less...';
$string['recent_activity_none']	= 'nothing new';

$string['recent_period_since_last_login'] = 'since last login ({$a})';
$string['recent_period_since_last_course_access'] = 'since last course visit ({$a})';
$string['since'] = 'last';
$string['years'] = 'years';
$string['year'] = 'year';
$string['months'] = 'months';
$string['month'] = 'month';
$string['weeks'] = 'weeks';
$string['week'] = 'week';
$string['days'] = 'days';
$string['day'] = 'day';
$string['hours'] = 'hours';
$string['hour'] = 'hour';
$string['minutes'] = 'minutes';
$string['minute'] = 'minute';
$string['seconds'] = 'seconds';
$string['second'] = 'second';
$string['today'] = 'today';
$string['yesterday'] = 'yesterday';
$string['daylapsesingle'] = '1 day ago';
$string['dayslapse'] = '{$a} days ago';
$string['minlapsesingle'] = '1 minute ago';
$string['minslapse'] = '{$a} minutes ago';
$string['seclapsesingle'] = 'just now';
$string['secslapse'] = '{$a} seconds ago';
$string['hrlapsesingle'] = '1 hour ago';
$string['hrslapse'] = '{$a} hours ago';
$string['weeklapsesingle'] = '1 week ago';
$string['weekslapse'] = '{$a} weeks ago';
$string['yrlapsesingle'] = '1 year ago';
$string['yrslapse'] = '{$a} years ago';
$string['mnthlapsesingle'] = '1 month ago';
$string['mnthslapse'] = '{$a} months ago';

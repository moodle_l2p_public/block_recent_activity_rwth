<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_recent_activity_rwth', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_recent_activity_rwth
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Neue Aktivitäten';

$string['privacy:metadata'] = 'Der letzte Aktivitäten-Block enthält einen Cache mit Daten, die an anderen Stellen in Moodle gespeichert wurden.';
$string['privacy:metadata:block_recent_activity_rwth'] = 'Temporärer Log früherer Traineraktivitäten. Wird nach zwei Tagen gelöscht.';
$string['privacy:metadata:block_recent_activity_rwth:action'] = 'Aktion: erstellt, aktualisiert oder gelöscht';
$string['privacy:metadata:block_recent_activity_rwth:cmid'] = 'Kursaktivitäts-ID';
$string['privacy:metadata:block_recent_activity_rwth:courseid'] = 'Kurs-ID';
$string['privacy:metadata:block_recent_activity_rwth:modname'] = 'Modultypbezeichung (für Löschaktion)';
$string['privacy:metadata:block_recent_activity_rwth:timecreated'] = 'Zeitpunkt der Ausführung der Aktion';
$string['privacy:metadata:block_recent_activity_rwth:userid'] = 'Nutzer der die Aktion ausführte';

$string['recent_activity_rwth:addinstance'] = 'Block \'Neue Aktivitäten\' hinzufügen';
$string['recent_activity_rwth:myaddinstance'] = 'Block \'Neue Aktivitäten\' hinzufügen';
$string['recent_activity_rwth:viewaddupdatemodule'] = 'Hinzugefügte und aktualisierte Module im Block \'Letzte Aktivitäten\' anzeigen.';
$string['recent_activity_rwth:viewdeletemodule'] = 'Gelöschte Module im Block \'Letzte Aktivitäten\' anzeigen.';

$string['max_recent_period'] = 'Maximale Zeitspanne für neue Aktivitäten';
$string['max_recent_period_desc'] = 'Die maximale Zeitspanne, in der neue Aktivitäten angezeigt werden. Format: <a href="https://www.php.net/manual/en/dateinterval.construct.php">https://www.php.net/manual/en/dateinterval.construct.php</a>';
$string['recent_periods'] = 'Zeitspannen';
$string['recent_periods_desc'] = 'Zeitspannen (Komma-getrennt), in denen neue Aktivitäten angezeigt werden. Format: <a href="https://www.php.net/manual/en/dateinterval.construct.php">https://www.php.net/manual/en/dateinterval.construct.php</a>';
$string['recent_activity_more'] = 'ausklappen...';
$string['recent_activity_less'] = 'einklappen...';
$string['recent_activity_none']	= 'keine Neuigkeiten';

$string['recent_period_since_last_login'] = 'seit letztem Login ({$a})';
$string['recent_period_since_last_course_access'] = 'seit letztem Kursaufruf ({$a})';
$string['since'] = 'seit';
$string['years'] = 'Jahren';
$string['year'] = 'Jahr';
$string['months'] = 'Monaten';
$string['month'] = 'Monat';
$string['weeks'] = 'Wochen';
$string['week'] = 'Woche';
$string['days'] = 'Tagen';
$string['day'] = 'Tag';
$string['hours'] = 'Stunden';
$string['hour'] = 'Stunde';
$string['minutes'] = 'Minuten';
$string['minute'] = 'Minute';
$string['seconds'] = 'Sekunden';
$string['second'] = 'Sekunde';
$string['today'] = 'heute';
$string['yesterday'] = 'gestern';
$string['daylapsesingle'] = 'vor 1 Tag';
$string['dayslapse'] = 'vor {$a} Tagen';
$string['minlapsesingle'] = 'vor 1 Minute';
$string['minslapse'] = 'vor {$a} Minuten';
$string['seclapsesingle'] = 'gerade jetzt';
$string['secslapse'] = 'vor {$a} Sekunden';
$string['hrlapsesingle'] = 'vor 1 Stunde';
$string['hrslapse'] = 'vor {$a} Stunden';
$string['weeklapsesingle'] = 'vor 1 Woche';
$string['weekslapse'] = 'vor {$a} Wochen';
$string['yrlapsesingle'] = 'vor 1 Jahr';
$string['yrslapse'] = 'vor {$a} Jahren';
$string['mnthlapsesingle'] = 'vor 1 Monat';
$string['mnthslapse'] = 'vor {$a} Monaten';

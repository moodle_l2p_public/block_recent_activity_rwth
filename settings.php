<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     block_recent_activity_rwth
 * @category    admin
 * @copyright   2018 Tim Schroeder <tim.schroeder@itc.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$settings->add(new admin_setting_configtext(
    'block_recent_activity_rwth/recent_periods',
    get_string('recent_periods', 'block_recent_activity_rwth'),
    get_string('recent_periods_desc', 'block_recent_activity_rwth'),
    'P3D,P1W,P2W')
);

$settings->add(new admin_setting_configtext(
    'block_recent_activity_rwth/max_recent_period',
    get_string('max_recent_period', 'block_recent_activity_rwth'),
    get_string('max_recent_period_desc', 'block_recent_activity_rwth'),
    'P6M')
);